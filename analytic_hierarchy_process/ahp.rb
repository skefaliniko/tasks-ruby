local_alternatives_priority = []
local_criteria_priority = []

source_table = [
    [120, 70, 90, 67, 81, 67, 110],
    [200, 100, 300, 600, 600, 300, 200],
    [9, 8, 8, 6, 8, 6, 7],
    [6, 5, 0, 5, 6, 3, 5],
    [1, 4, 9, 0, 0, 2, 0],
    [7.9, 3.9, 4, 3.2, 3.8, 3.6, 8.4]
]

loop_length = source_table.size

for times in 0..loop_length do

    criteria_preferences_arr = []
   
    variants_array = [["D1",nil],["D2",nil],["D3",nil],["D4",nil],["D5",nil],["D6",nil],["D7",nil]]
    if times == loop_length
        File.open('criteria-preferences.txt', 'r') do |file|
            file.each_line {|criteria|
                criteria_preferences_arr = criteria.split.map(&:to_s)
            }
        end
    else
        temp_pref = source_table[times]
        variants_array.each_index { |i|
            variants_array[i][1] = temp_pref[i]
        }
        temp_pref.sort!

        variants_array.find_all { |key, value|
            temp_pref.each_index { |crit_i|
                if value == temp_pref[crit_i]
                    criteria_preferences_arr[crit_i] = key.to_s
                end
            }
            puts "#{key} -> #{value}"
        }
    end

    puts "Предпочтения"
    print criteria_preferences_arr
    puts


    marks = []
    criteria_preferences_arr.size == 5 ? marks = [1,3,5,7,9] : marks = [1,2,3,4,5,6,7,8,9]

    criteria = []
    criteria_preferences_arr.each_index { |i|
        tmp = []
        tmp[0] = criteria_preferences_arr[i]
        tmp[1] = marks[i]
        criteria << tmp
    }

    criteria_table = []
    tmp_preferences = criteria_preferences_arr
    criteria_preferences_arr.each_index { |i|
        tmp = []
        tmp[0] = criteria_preferences_arr[i]
        criteria_preferences_arr.each_index { |j|
            tmp2 = []
            criteria_preferences_arr.each_index { |k| 
                tmp3 = []
                tmp3[0] = criteria_preferences_arr[k]
                tmp3[1] = nil
                tmp2 << tmp3
            }
            tmp[1] = tmp2
        }
        criteria_table << tmp
    }

    firts_count = criteria_preferences_arr.size - 1
    criteria_table.each_index { |i|
        criteria_table[i].each_index { |j|
            if criteria_table[i][j].class != String
                count = firts_count
                criteria_table[i][j].each_index { |k|
                    criteria_table[i][j][k][1] = criteria_preferences_arr.size - count
                    count -= 1
                }
                firts_count += 1
            end
        }
    }


    criteria_table.each_index { |i|
        criteria_table[i].each_index { |j|
            if criteria_table[i][j].class != String
                criteria_table[i][j].each_index { |k|
                    criteria_table[i][j][k][1] = 0 if criteria_table[i][j][k][1] < 0
                }
            end
        }
    }

    criteria_table.each_index { |i|
        if criteria_table[i].class == String
            criteria_table[i].sort!
        end
    }

    super_arr = Array.new(criteria_preferences_arr.size) { |i| i = nil }
    super_criteria = criteria_preferences_arr.sort

    criteria_table.find_all { |key, value_arr|
        super_criteria.each_index { |crit_i|
            if key == super_criteria[crit_i]
                super_arr[crit_i] = value_arr
            end
        }
    }

    criteria_table = super_arr

    arr = []
    criteria_table.each_index { |i|
        super_arr = []
        criteria_table[i].find_all { |key, value_arr|
            super_criteria.each_index { |crit_i|
                if key == super_criteria[crit_i]
                    super_arr[crit_i] = value_arr
                end
            }
        }
        arr << super_arr
    }

    arr.each_index { |i|
        arr.each_index { |j|
            arr[j][i] = Rational(1, arr[i][j]) if arr[j][i] == 0
        }
    }

    #Итоговая матрица критериев 
    arr.map { |e|
        e.map { |inner_e| 
            print "#{inner_e}\t"
        }
        puts
    }
    puts

    components_vector = []
    arr.each_index { |i|
        tmp_elem = 1
        arr[i].each_index { |j|
            tmp_elem *= arr[i][j]
        }
        components_vector[i] = tmp_elem ** (1.0/criteria_preferences_arr.size)
    }

    summ_of_components_vector = 0
    components_vector.map { |e| summ_of_components_vector += e }

    normalized_components_vector = components_vector.map { |e|
        e / summ_of_components_vector
    }

    #Сохранение в итоговый массив для рассчета приоритета вариантов
    if times == loop_length 
        local_criteria_priority = normalized_components_vector
    else
        local_alternatives_priority << normalized_components_vector
    end
        

    extra_criteries_vector = []
    arr.each_index { |i|
        sum = 0
        arr.each_index { |j|
            sum += arr[j][i]
        }
        extra_criteries_vector[i] = sum * normalized_components_vector[i]
    }

    lambda_max = 0
    extra_criteries_vector.map { |e| 
        lambda_max += e
    }
    n_n = criteria_preferences_arr.size
    i_s = (lambda_max - n_n)/(n_n - 1)
    s_s = 1.98 * (n_n - 2) / n_n
    o_s = i_s / s_s

    puts "components \n #{components_vector}"
    puts
    puts "components_vector_sum \n #{summ_of_components_vector}"
    puts
    puts "normalized_components_vector \n #{normalized_components_vector}"
    puts
    puts "extra_criteries_vector \n #{extra_criteries_vector}"
    puts
    puts "lambda_max \n #{lambda_max}"
    puts
    puts "N \n #{n_n}"
    puts
    puts "ИС \n #{i_s}"
    puts
    puts "СС \n #{s_s}"
    puts
    print "ОС \n #{o_s}"
    print " - результат допустимый \n" if o_s < 0.2
    puts
    local_alternatives_priority.map { |e| puts "--> #{e}\n" }
    puts "\n==> #{local_criteria_priority}"

end #end of for times.. loop

result_array = [["D1",nil],["D2",nil],["D3",nil],["D4",nil],["D5",nil],["D6",nil],["D7",]]

final_sort_arr = []

tmp_super_table = []

local_alternatives_priority.each_index { |i|
    temp = []
    local_alternatives_priority.each_index { |j|
        temp << local_alternatives_priority[j][i]
    }
    tmp_super_table << temp
}

extra_column = []
extra_column << local_alternatives_priority[0][6]
extra_column << local_alternatives_priority[1][6]
extra_column << local_alternatives_priority[2][6]
extra_column << local_alternatives_priority[3][6]
extra_column << local_alternatives_priority[4][6]
extra_column << local_alternatives_priority[5][6]
tmp_super_table << extra_column

puts
print "SuperT\n"
    tmp_super_table.map { |e| puts "--> #{e}\n" }

puts

tmp_super_table.each_index { |i| 
    sum = 0.0
    tmp_super_table.each_index { |j|
        unless tmp_super_table[i][j].class == NilClass
            temp = tmp_super_table[i][j] * local_criteria_priority[j]
            sum += temp
        end
    }
    final_sort_arr << sum
    result_array[i][1] = sum
}

print "#{result_array}\n"
print "\n\nfinal sort arr #{final_sort_arr}\n\n"
local_alternatives_priority.map { |e| puts "--> #{e}\n" }

final_sort_arr.sort!
result = []

result_array.find_all { |key, value|
    final_sort_arr.each_index { |crit_i|
        if value == final_sort_arr[crit_i]
            result[crit_i] = key
        end
    }
}

puts
puts "ФИНАЛЬНЫЙ РЕЗУЛЬТАТ!!"
print result
puts

